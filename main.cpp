/*
 * Coin2Coin (https://bitbucket.org/netburnerinterns/coin2coin)
 * Copyright (c) 2015 by
 *
 * Sam Posner (http://arcadeoftheabsurd.com/)
 * and NetBurner, Inc. (http://www.netburner.com/)
 *
 * Distributed under the BSD 3-Clause License
 * See LICENSE.txt or visit http://opensource.org/licenses/BSD-3-Clause
 */

#include <predef.h>
#include <constants.h>
#include <ucos.h>
#include <system.h>
#include <init.h>
#include <iosys.h>
#include <serial.h>
#include <pins.h>
#include <string.h>
#include <HiResTimer.h>

#include "lib/libqrencode/qrencode.h"
#include "https_client.h"

#include "keygen.h"
#include "printer.h"
#include "blockchain_info.h"

#define UART_NUMBER 1
#define RX_PIN 21
#define TX_PIN 22
#define GPIO_PIN 15

#define PULSE_TIME .1
#define QUARTER_PULSES 10

#define QR_SCALE 8
#define QR_SCALED_SIZE 8192
#define SATOSHIS_PER_BTC 100000000

extern "C" {
    void UserMain    (void * pd);
    void HTMLGetQRWidth(int fd, PCSTR url);
    void HTMLGetQRJavaScript (int fd, PCSTR url);
    void HTMLGetMiniPrivateKey (int fd, PCSTR url);
}

const char * AppName="Coin2Coin";

static QRcode *qr_priv;
static QRcode *qr_addr;
static int printer;
static char js_buffer[16384];
static char qr_scaled[QR_SCALED_SIZE];
static bool draw_ready = false;
static bool got_keys = false;

static const char msg_priv_1[] = "Private key (WIF):";
static const char msg_priv_2[] = "Private key (mini):";
static const char msg_addr[] = "Public Bitcoin address:";
static const char msg_credits_1[] = "By @smpsnr (arcadeoftheabsurd.com)";
static const char msg_credits_2[] = "Powered by NetBurner";

/*
 * Prints the scaled width of the QR code to the html page
 */
void HTMLGetQRWidth(int fd, PCSTR url) {
    if (qr_priv != 0) {
        char buf[255];
        siprintf(buf, "%i", qr_priv->width * QR_SCALE);
        writestring(fd, buf);
    }
}

/*
 * Prints the generated JavaScript to the html page
 */
void HTMLGetQRJavaScript (int fd, PCSTR url) {
    if (draw_ready) {
        writestring(fd, js_buffer);
    }
}

/*
 * Prints the Bitcoin mini private key to the html page
 */
void HTMLGetMiniPrivateKey(int fd, PCSTR url) {
    if (got_keys) {
        writestring(fd, GetMiniPrivateKey());
    }
}

/*
 * Resizes QR code using nearest neighbor interpolation
 */
void ResizeQRCode(unsigned char* data_in, char data_out[], int size1, int size2) {
    int ratio = (int)((size1 << 16) / size2) + 1;

    for (int y2 = 0; y2 < size2; y2++) {
        for (int x2 = 0; x2 < size2; x2++) {
            int x1 = (x2 * ratio) >> 16;
            int y1 = (y2 * ratio) >> 16;
            data_out[x2 + (y2 * size2)] = data_in[x1 + (y1 * size1)];
        }
    }
}

double GetBTC(char* value) {
    char rx_buffer[1000];
    char request[256];

    GetPriceRequest(request, "USD", value);
    HTTPSRequest(BLOCKCHAIN_HOST, request, rx_buffer, 1000);
    return ParsePrice(rx_buffer);
}

void SendMoney(long satoshis) {
    char rx_buffer[2000];
    char request[256];

    GetPaymentRequest(request, GetAddress(), satoshis);

    //iprintf("%s\n", request);

    HTTPSRequest(BLOCKCHAIN_HOST, request, rx_buffer, 2000);

    if (ParseError(GetJSON(rx_buffer))) {
        iprintf("\nError sending BTC\n");
    } else {
        iprintf("\nTransaction successful\n");
    }
}

void Go() {
    iprintf("\nStarting Keygen task\n\n");
    StartKeygenTask();

    WaitKeygenTask();

    got_keys = true;

    iprintf("\nMini private key: %s\n", GetMiniPrivateKey());
    iprintf("WIF private key: %s\n", GetWIFKey());
    iprintf("Address: %s\n\n", GetAddress());

    iprintf("Getting exchange rate\n\n");

    double btc = GetBTC(".25");
    long satoshis = (long) (btc * SATOSHIS_PER_BTC);

    iprintf("\nSending %li satoshis (.25 USD)\n\n", satoshis);

    SendMoney(satoshis);

    // generate QR code

    iprintf("\nGenerating QR Code\n");

    qr_priv = QRcode_encodeString(GetMiniPrivateKey(), 2, QR_ECLEVEL_L, QR_MODE_8, true);
    qr_addr = QRcode_encodeString(GetAddress(),        2, QR_ECLEVEL_L, QR_MODE_8, true);

    iprintf("Generating JavaScript\n");

    int size = qr_priv->width;
    int offset = 0;

    // generate JavaScript

    for(int x = 0; x < size; x++) {
        for (int y = 0; y < size; y++) {
            if (qr_priv->data[x + (y * size)] & 0x01) {
                offset += siprintf(js_buffer + offset, "ctx.rect(%i, %i, %i, %i); ctx.fill();\n", x * QR_SCALE, y * QR_SCALE, QR_SCALE, QR_SCALE);
            }
        }
    }

    draw_ready = true;

    // print QR code with thermal printer

    iprintf("\nSetting up printer\n");

    SetPrintMode(printer, FONT_A);
    SetPrintSize(printer, SIZE_LARGE);
    SetPrintJustify(printer, JUSTIFY_CENTER);

    char buf[32];
    snprintf(buf, 32, "%g BTC", btc);

    write(printer, buf, strlen(buf));
    PrintParagraph(printer);

    // private key

    int size_scaled = qr_priv->width * QR_SCALE;
    ResizeQRCode(qr_priv->data, qr_scaled, qr_priv->width, size_scaled);

    iprintf("Printing private key\n");

    SetPrintSize(printer, SIZE_SMALL);
    SetPrintJustify(printer, JUSTIFY_LEFT);

    write(printer, msg_priv_1, strlen(msg_priv_1));
    PrintParagraph(printer);

    SetPrintMode(printer, FONT_B_BOLD);

    write(printer, GetWIFKey(), strlen(GetWIFKey()));
    PrintParagraph(printer);

    SetPrintMode(printer, FONT_A);

    write(printer, msg_priv_2, strlen(msg_priv_2));
    PrintParagraph(printer);

    // give the printer a break
    OSTimeDly(TICKS_PER_SECOND * 2);

    PrintQRCode(printer, qr_scaled, size_scaled);

    // clear qr_scaled buffer
    memset(qr_scaled, 0, QR_SCALED_SIZE);

    PrintDividerLine(printer);
    PrintParagraph(printer);

    // give the printer a break
    OSTimeDly(TICKS_PER_SECOND * 2);

    // public address

    size_scaled = qr_addr->width * QR_SCALE;
    ResizeQRCode(qr_addr->data, qr_scaled, qr_addr->width, size_scaled);

    iprintf("Printing public address\n");

    write(printer, msg_addr, strlen(msg_addr));
    PrintParagraph(printer);

    SetPrintMode(printer, FONT_B_BOLD);

    write(printer, GetAddress(), strlen(GetAddress()));
    PrintParagraph(printer);

    PrintQRCode(printer, qr_scaled, size_scaled);

    // clear qr_scaled buffer
    memset(qr_scaled, 0, QR_SCALED_SIZE);

    SetPrintMode(printer, FONT_A);

    PrintDividerLine(printer);

    SetPrintJustify(printer, JUSTIFY_CENTER);
    SetPrintMode(printer, FONT_B);

    write(printer, msg_credits_1, strlen(msg_credits_1));
    PrintNewline(printer);
    write(printer, msg_credits_2, strlen(msg_credits_2));

    PrintParagraph(printer);
    PrintParagraph(printer);

    SetPrintJustify(printer, JUSTIFY_LEFT);
}

/*
 * Main
 */
void UserMain(void * pd) {
    initWithWeb();

    iprintf("\nApplication started\n");

    // serial printer setup

    SerialClose(UART_NUMBER);

    printer = SimpleOpenSerial(UART_NUMBER, PRINTER_BAUDRATE);

    J2[RX_PIN].function(PINJ2_21_UART1_RXD);
    J2[TX_PIN].function(PINJ2_22_UART1_TXD);

    SetupPrinter(printer);

    // coin acceptor GPIO

    J2[GPIO_PIN].function(PINJ2_15_GPIO);
    HiResTimer* timer = HiResTimer::getHiResTimer();

    int counter;

    while (1) {
        while(!J2[GPIO_PIN].read()) {
            timer->pollingDelay(PULSE_TIME);
        }

        counter = 0;

        while(J2[GPIO_PIN].read()) {
            counter++;
            timer->pollingDelay(PULSE_TIME*2);
        }

        // assuming the coin acceptor was programmed to pulse 10 times for a quarter
        if (counter == QUARTER_PULSES) {
            iprintf("\nQuarter accepted\n");
            Go();
        }
    }
}
