#include <predef.h>
#include <constants.h>
#include <ucos.h>
#include <iosys.h>
#include <crypto/random.h>
#include <crypto/SHA2.h>

#include "lib/libbase58/libbase58.h"
#include "lib/secp256k1/secp256k1.h"
#include "lib/bitcoin/crypto/ripemd160.h"

#include "test.h"

#define TEST_STACK_SIZE 262144
#define TEST_TASK_PRIORITY 46

asm( " .align 4 " );

DWORD TestStack[TEST_STACK_SIZE] __attribute__( ( aligned( 4 ) ) );

void PrintHex(PBYTE data, int len) {
    for(int i = 0; i < len; i++) {
        printf("%02x", data[i]);
    }
}

void RunCryptoTests() {
    iprintf("\nTesting secp256k1 public key generation\n");

    unsigned char pub[65];
    unsigned char priv[] = {
        0x18, 0xE1, 0x4A, 0x7B, 0x6A, 0x30,
        0x7F, 0x42, 0x6A, 0x94, 0xF8, 0x11, 0x47,
        0x01, 0xE7, 0xC8, 0xE7, 0x74, 0xE7, 0xF9,
        0xA4, 0x7E, 0x2C, 0x20, 0x35, 0xDB, 0x29,
        0xA2, 0x06, 0x32, 0x17, 0x25
    };

    secp256k1_context_t* ctx_ecc = secp256k1_context_create(SECP256K1_CONTEXT_SIGN);
    int pub_len = 0;

    if (secp256k1_ec_pubkey_create(ctx_ecc, pub, &pub_len, priv, false)) {
        iprintf("result: ");
        PrintHex(pub, pub_len);

        iprintf("\nexpect: 0450863ad64a87ae8a2fe83c1af1a8403cb53f53e486d8511dad8a04887e5b23522cd470243453a299fa9e77237716103abc11a1df38855ed6f2ee187e9c582ba6\n");
    } else {
        iprintf("\nfailed to create public key\n");
    }

    secp256k1_context_destroy(ctx_ecc);

    iprintf("\nTesting SHA-256 hashing\n");

    SHA2_CTX ctx_sha;
    unsigned char data_sha[] = "abc";
    unsigned char hash_sha[32];

    SHA2Init(&ctx_sha, false);
    SHA2Update(&ctx_sha, data_sha, 3);
    SHA2Final(hash_sha, &ctx_sha);

    iprintf("result: ");
    PrintHex(hash_sha, 32);

    iprintf("\nexpect: ba7816bf8f01cfea414140de5dae2223b00361a396177a9cb410ff61f20015ad\n\n");

    iprintf("Testing RIPEMD-160 hashing\n");

    unsigned char data_ripemd[] = "abc";
    unsigned char hash_ripemd[20];

    CRIPEMD160 hasher = CRIPEMD160();
    hasher.Write(data_ripemd, 3);
    hasher.Finalize(hash_ripemd);

    iprintf("result: ");
    PrintHex(hash_ripemd, 20);

    iprintf("\nexpect: 8eb208f7e05d987a9b044a8e98c6b087f15a0bfc");
}

void RunBitcoinAddressTest() {
    // Test vectors taken from
    // https://en.bitcoin.it/wiki/Technical_background_of_version_1_Bitcoin_addresses

    iprintf("\nTesting Bitcoin address creation\n");

    iprintf("\nGenerating secp256k1 public key\n");

    unsigned char pub[65];
    unsigned char priv[] = {
        0x18, 0xE1, 0x4A, 0x7B, 0x6A, 0x30,
        0x7F, 0x42, 0x6A, 0x94, 0xF8, 0x11, 0x47,
        0x01, 0xE7, 0xC8, 0xE7, 0x74, 0xE7, 0xF9,
        0xA4, 0x7E, 0x2C, 0x20, 0x35, 0xDB, 0x29,
        0xA2, 0x06, 0x32, 0x17, 0x25
    };

    secp256k1_context_t* ctx_ecc = secp256k1_context_create(SECP256K1_CONTEXT_SIGN);
    int pub_len = 0;

    if (secp256k1_ec_pubkey_create(ctx_ecc, pub, &pub_len, priv, false)) {
        iprintf("result: ");
        PrintHex(pub, pub_len);

        iprintf("\nexpect: 0450863ad64a87ae8a2fe83c1af1a8403cb53f53e486d8511dad8a04887e5b23522cd470243453a299fa9e77237716103abc11a1df38855ed6f2ee187e9c582ba6\n");
    } else {
        iprintf("\nfailed to create public key\n");
    }

    secp256k1_context_destroy(ctx_ecc);

    iprintf("\nSHA-256 hashing public key\n");

    SHA2_CTX ctx_sha;
    unsigned char hash_sha[32];

    SHA2Init(&ctx_sha, false);
    SHA2Update(&ctx_sha, pub, pub_len);
    SHA2Final(hash_sha, &ctx_sha);

    iprintf("result: ");
    PrintHex(hash_sha, 32);

    iprintf("\nexpect: 600ffe422b4e00731a59557a5cca46cc183944191006324a447bdb2d98d4b408\n\n");

    iprintf("RIPEMD-160 hashing result\n");

    unsigned char hash_ripemd[25];

    CRIPEMD160 hasher = CRIPEMD160();
    hasher.Write(hash_sha, 32);
    hasher.Finalize(hash_ripemd + 1);

    iprintf("result: ");
    PrintHex(hash_ripemd + 1, 20);

    iprintf("\nexpect: 010966776006953d5567439e5e39f86a0d273bee\n");

    iprintf("\nAppending version byte\n");

    hash_ripemd[0] = 0x00;

    iprintf("result: ");
    PrintHex(hash_ripemd, 21);

    iprintf("\nexpect: 00010966776006953d5567439e5e39f86a0d273bee\n");

    iprintf("\nBase58Check encoding\n");

    iprintf("\nSHA-256 hashing extended result\n");

    SHA2Init(&ctx_sha, false);
    SHA2Update(&ctx_sha, hash_ripemd, 21);
    SHA2Final(hash_sha, &ctx_sha);

    iprintf("result: ");
    PrintHex(hash_sha, 32);

    iprintf("\nexpect: 445c7a8007a93d8733188288bb320a8fe2debd2ae1b47f0f50bc10bae845c094\n");

    iprintf("\nSHA-256 hashing hash\n");

    SHA2Init(&ctx_sha, false);
    SHA2Update(&ctx_sha, hash_sha, 32);
    SHA2Final(hash_sha, &ctx_sha);

    iprintf("result: ");
    PrintHex(hash_sha, 32);

    iprintf("\nexpect: d61967f63c7dd183914a4ae452c9f6ad5d462ce3d277798075b107615c1a8a30\n");

    iprintf("\nGetting 4 byte checksum\n");

    unsigned char checksum[4];

    checksum[0] = hash_sha[0];
    checksum[1] = hash_sha[1];
    checksum[2] = hash_sha[2];
    checksum[3] = hash_sha[3];

    iprintf("result: ");
    PrintHex(checksum, 4);

    iprintf("\nexpect: d61967f6\n");

    iprintf("\nAppending checksum to extended RIPEMD-160 hash\n");

    hash_ripemd[21] = checksum[0];
    hash_ripemd[22] = checksum[1];
    hash_ripemd[23] = checksum[2];
    hash_ripemd[24] = checksum[3];

    iprintf("result: ");
    PrintHex(hash_ripemd, 25);

    iprintf("\nexpect: 00010966776006953d5567439e5e39f86a0d273beed61967f6\n");

    iprintf("\nConverting result to a base58 Bitcoin address\n");

    char out_str[128];
    int out_len;

    b58enc(out_str, &out_len, hash_ripemd, 25);

    iprintf("result: %s\n", out_str);
    iprintf("expect: 16UwLL9Risc3QfPqBUvKofHmBQ7wMtjvM\n");
}

void TestTask(void * pdata) {
    RunCryptoTests();
    RunBitcoinAddressTest();
}

void StartTestTask() {
    if (OSTaskCreate(TestTask,
                     NULL,
                     (void*)&TestStack[TEST_STACK_SIZE],
                     (void *)TestStack, TEST_TASK_PRIORITY
                    )!=OS_NO_ERR) {
        iprintf("Error starting task\n");
    }
}
