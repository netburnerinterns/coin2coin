/*
 * Coin2Coin (https://bitbucket.org/netburnerinterns/coin2coin)
 * Copyright (c) 2015 by
 *
 * Sam Posner (http://arcadeoftheabsurd.com/)
 * and NetBurner, Inc. (http://www.netburner.com/)
 *
 * Distributed under the BSD 3-Clause License
 * See LICENSE.txt or visit http://opensource.org/licenses/BSD-3-Clause
 */

/*
 * Partial driver for the CSN-A2-T thermal printer
 *
 * First, open a file descriptor to the printer serial port
 * Make sure the baud rate defined in printer.h is correct for your printer
 *
 * Print functions take the file descriptor as their first argument
 * Call SetupPrinter before you print anything
 * To print text, just write directly to the file descriptor
 *
 * For documentation, see the "A2 Micro panel thermal printer" user manual:
 * https://www.sparkfun.com/datasheets/Components/General/A2-user%20manual-1.pdf
 */

#ifndef PRINTER_H_
#define PRINTER_H_

// baud rate may be different on individual printers. check the debug printout

#define PRINTER_BAUDRATE 19200

// printer command parameters

#define FONT_A 0x00
#define FONT_B 0x01
#define FONT_B_BOLD 0x21

#define SIZE_SMALL 0x00
#define SIZE_MEDIUM 0x01
#define SIZE_LARGE 0x11

#define JUSTIFY_LEFT 0
#define JUSTIFY_CENTER 1
#define JUSTIFY_RIGHT 2

void SetupPrinter(int fd);

void PrintNewline(int fd);
void PrintParagraph(int fd);
void PrintDividerLine(int fd);

void SetPrintMode(int fd, char mode);
void SetPrintSize(int fd, char size);
void SetPrintJustify(int fd, char justify);

void PrintQRCode(int fd, char data[], int size);
void PrintBitmap(int fd, const unsigned char* data, int width, int height);

#endif /* PRINTER_H_ */
