/*
 * Modify this file with your blockchain.info GUID and password
 * Make sure they are URL encoded
 */

#ifndef APIKEYS_H_
#define APIKEYS_H_

#define GUID "GUID-goes-here"
#define PASS "password-goes-here"

#endif /* APIKEYS_H_ */
