/*
 * Coin2Coin (https://bitbucket.org/netburnerinterns/coin2coin)
 * Copyright (c) 2015 by
 *
 * Sam Posner (http://arcadeoftheabsurd.com/)
 * and NetBurner, Inc. (http://www.netburner.com/)
 *
 * Distributed under the BSD 3-Clause License
 * See LICENSE.txt or visit http://opensource.org/licenses/BSD-3-Clause
 */

/*
 * Library to compose requests and parse responses from
 * the blockchain.info api: https://blockchain.info/api
 *
 * Define blockchain.info GUID and password in apikeys.h
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "blockchain_info.h"
#include "apikeys.h"

/*
 * Composes a request for the balance of the wallet associated with this GUID
 */
void GetWalletBalanceRequest(char out[]) {
    snprintf(out, strlen(out),
        "GET /merchant/" GUID "/balance?password=" PASS " HTTP/1.1\r\n"
        "Host: " BLOCKCHAIN_HOST "\r\n\r\n"
    );
}

/*
 * Composes a request for the balance of a given address,
 * including transactions with at least the given number of block confirmations
 */
void GetAddressBalanceRequest(char out[], char* address, int confirmations) {
    snprintf(out, strlen(out),
        "GET /merchant/" GUID "/address_balance?password=" PASS "&address=%s&confirmations=%i HTTP/1.1\r\n"
        "Host: " BLOCKCHAIN_HOST "\r\n\r\n",
        address, confirmations
    );
}

/*
 * Composes a request that transfers a given amount of bitcoin to a given address
 * Note - bitcoin value is specified in 100 millionth BTC (ie the satoshi)
 */
void GetPaymentRequest(char out[], char* address, long amount) {
    snprintf(out, strlen(out),
        "GET /merchant/" GUID "/payment?password=" PASS "&to=%s&amount=%li HTTP/1.1\r\n"
        "Host: " BLOCKCHAIN_HOST "\r\n\r\n",
        address, amount
    );
}

/*
 * Composes a request for the value in bitcoin of a value of a specified currency
 */
void GetPriceRequest(char out[], char* currency, char* value) {
    snprintf(out, strlen(out),
        "GET /tobtc?currency=%s&value=%s HTTP/1.1\r\n"
        "Host: " BLOCKCHAIN_HOST "\r\n\r\n",
        currency, value
    );
}

/*
 * Parses the response to a price request; returns the price
 */
double ParsePrice(char* rx_buffer) {
    char* price = strstr(rx_buffer, "LAX\r\n") + 5;
    return atof(price);
}

/*
 * Parses a JSON response; returns true if there is an error
 */
bool ParseError(cJSON* root) {
    if (root) {
        cJSON* error = cJSON_GetObjectItem(root, "error");

        if (error) {
            iprintf("%s\n", error->valuestring);
            return true;
        }

        return false;
    }

    return true;
}

/*
 * Parses the response to a balance request;
 * returns the wallet balance, or -1 if there was an error
 */
double ParseBalance(cJSON* root) {
    if(!ParseError(root)) {
        cJSON* balance = cJSON_GetObjectItem(root, "balance");

        if (balance) {
            return balance->valuedouble;
        }
    }

    return -1;
}

// couldn't get post requests to work

/*void PostPaymentRequest(char out[], char* address, char* amount) {
    char content[128];

    snprintf(content, strlen(content),
        "password=" PASS "&to=%s&amount=%s",
        address, amount
    );
    int len = strlen(content);

    snprintf(out, strlen(out),
        "POST /merchant/" GUID "/payment HTTP/1.1\r\n"
        "Host: blockchain.info\r\n"
        "Content-Type: application/x-www-form-urlencoded\r\n"
        "Content-Length: %i\r\n"
        "%s\r\n\r\n",
        len, content
    );
}*/
