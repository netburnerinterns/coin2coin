/*
 * Coin2Coin (https://bitbucket.org/netburnerinterns/coin2coin)
 * Copyright (c) 2015 by
 *
 * Sam Posner (http://arcadeoftheabsurd.com/)
 * and NetBurner, Inc. (http://www.netburner.com/)
 *
 * Distributed under the BSD 3-Clause License
 * See LICENSE.txt or visit http://opensource.org/licenses/BSD-3-Clause
 */

/*
 * Partial driver for the CSN-A2-T thermal printer
 *
 * First, open a file descriptor to the printer serial port
 * Make sure the baud rate defined in printer.h is correct for your printer
 *
 * Print functions take the file descriptor as their first argument
 * Call SetupPrinter before you print anything
 * To print text, just write directly to the file descriptor
 *
 * For documentation, see the "A2 Micro panel thermal printer" user manual:
 * https://www.sparkfun.com/datasheets/Components/General/A2-user%20manual-1.pdf
 */

#include <ctype.h>
#include <startnet.h>

#include "printer.h"

// ASCII codes for printer commands

#define ASCII_ESC 27
#define ASCII_GS  29
#define ASCII_DC2 18

// printer constants

#define PRINT_BUFFER  256
#define BYTES_PER_ROW  48

// 2-byte sequences specifying printer commands

#define CMD_PRINT_MODE     (char[2]){ASCII_ESC, '!'}
#define CMD_PRINT_SIZE     (char[2]){ASCII_GS,  '!'}
#define CMD_PRINT_JUSTIFY  (char[2]){ASCII_ESC, 'a'}
#define CMD_CTRL_PARAMETER (char[2]){ASCII_ESC, '7'}
#define CMD_PRINT_DENSITY  (char[2]){ASCII_DC2, '#'}
#define CMD_PRINT_BITMAP   (char[2]){ASCII_DC2, '*'}

// setup values from SparkFun example code:
// https://www.sparkfun.com/products/10438

#define MAX_DOTS           7
#define HEAT_TIME        255
#define HEAT_INTERVAL    255
#define PRINT_DENSITY     15
#define PRINT_BREAK_TIME  15

// 32 '═' characters for pretty dividing line

static const char divider_line[] = {
    205, 205, 205, 205, 205, 205, 205, 205,
    205, 205, 205, 205, 205, 205, 205, 205,
    205, 205, 205, 205, 205, 205, 205, 205,
    205, 205, 205, 205, 205, 205, 205, 205 };

/*
 * Writes a command to the printer
 * Print commands consist of 2 bytes followed by a variable number of arguments
 */
void WriteCommand(int fd, char command_bytes[], int num_parameters, char parameters[]) {
    write(fd, command_bytes, 2);
    write(fd, parameters, num_parameters);
}

/*
 * Sets printer parameters to preordained values
 * Call this function before printing anything else
 */
void SetupPrinter(int fd) {
    // set max heating dots, heating time, and heating interval
    WriteCommand(fd, CMD_CTRL_PARAMETER, 3, (char[3]){MAX_DOTS, HEAT_TIME, HEAT_INTERVAL});

    // set print density
    char density = (PRINT_DENSITY << 4) | PRINT_BREAK_TIME;
    WriteCommand(fd, CMD_PRINT_DENSITY, 1, &density);
}

/*
 * Writes a single byte to the printer
 */
void WriteByte(int fd, char byte) {
    write(fd, &byte, 1);
}

/*
 * Prints a newline
 */
void PrintNewline(int fd) {
    write(fd, "\n", 1);
}

/*
 * Prints two newlines
 */
void PrintParagraph(int fd) {
    write(fd, "\n\n", 2);
}

/*
 * Prints a line of '═' characters across the page
 */
void PrintDividerLine(int fd) {
    write(fd, divider_line, 32);
}

/*
 * Sets printer text mode (bold, font, etc.)
 */
void SetPrintMode(int fd, char mode) {
    WriteCommand(fd, CMD_PRINT_MODE, 1, &mode);
}

/*
 * Sets printer text size
 */
void SetPrintSize(int fd, char size) {
    WriteCommand(fd, CMD_PRINT_SIZE, 1, &size);
}

/*
 * Sets printer text justification
 * Note - only works for text, not bitmaps
 */
void SetPrintJustify(int fd, char justify) {
    WriteCommand(fd, CMD_PRINT_JUSTIFY, 1, &justify);
}

/*
 * Converts libqrencode QR code to thermal printer bitmap format and prints it
 * Size must be a multiple of 8 smaller than or equal to 384
 *
 * Adapted from AdaFruit Thermal Printer Library:
 * https://github.com/adafruit/Adafruit-Thermal-Printer-Library
 */
void PrintQRCode(int fd, char data[], int size) {
    int rows = size;
    int row_bytes = (size + 7) / 8;

    int cols = (row_bytes >= BYTES_PER_ROW) ? BYTES_PER_ROW : row_bytes;

    // break the bitmap up into 256 byte chunks
    // to avoid overflowing print buffer

    int rowsinchunk = PRINT_BUFFER / cols;

    if (rowsinchunk > PRINT_BUFFER-1) {
        rowsinchunk = PRINT_BUFFER - 1;
    }
    else if(rowsinchunk < 1) {
        rowsinchunk = 1;
    }

    // for each group of rows
    for (int rowstart = 0; rowstart < rows; rowstart += rowsinchunk) {
        int chunkrows = rows - rowstart;

        if (chunkrows > rowsinchunk) {
            chunkrows = rowsinchunk;
        }

        // begin printing new bitmap
        WriteCommand(fd, CMD_PRINT_BITMAP, 2, (char[2]){chunkrows, cols});

        // for each row
        for (int r = rowstart; r < rowstart + chunkrows; r++) {
            for (int c = 0; c < cols; c++) {

                // each byte of the output bitmap
                // corresponds to 8 bytes of QR code data
                char byte = 0x00;

                for (int k = 0; k < 8; k++) {
                    if(data[(c * 8) + (r * rows) + k] & 0x1) {
                        // if this pixel is set,
                        // set the corresponding bit of the output byte
                        byte |= (0x1 << k);
                    }
                }

                WriteByte(fd, byte);
            }
        }
    }
}

/*
 * Prints a bitmap. Width must be a multiple of 8 smaller than or equal to 384
 *
 * See Adafruit tutorial on converting bitmaps to correct format:
 * https://learn.adafruit.com/mini-thermal-receipt-printer/bitmap-printing
 *
 * Adapted from AdaFruit Thermal Printer Library:
 * https://github.com/adafruit/Adafruit-Thermal-Printer-Library
 */
void PrintBitmap(int fd, const unsigned char* data, int width, int height) {
    int rows = height;
    int row_bytes = (width + 7) / 8;

    int cols = (row_bytes >= BYTES_PER_ROW) ? BYTES_PER_ROW : row_bytes;

    // break the bitmap up into 256 byte chunks
    // to avoid overflowing print buffer

    int rowsinchunk = PRINT_BUFFER / cols;

    if (rowsinchunk > PRINT_BUFFER-1) {
        rowsinchunk = PRINT_BUFFER-1;
    }
    else if(rowsinchunk < 1) {
        rowsinchunk = 1;
    }

    int i = 0;

    // for each group of rows
    for (int rowstart = 0; rowstart < rows; rowstart += rowsinchunk) {
        int chunkrows = rows - rowstart;

        if (chunkrows > rowsinchunk) {
            chunkrows = rowsinchunk;
        }

        // begin printing new bitmap
        WriteCommand(fd, CMD_PRINT_BITMAP, 2, (char[2]){chunkrows, cols});

        // for each row
        for (int r = rowstart; r < rowstart + chunkrows; r++) {
            for (int c = 0; c < cols; c++, i++) {
                WriteByte(fd, *(data + i));
            }

            // if the width is clipped, skip to the next row
            i += row_bytes - cols;
        }
    }
}
