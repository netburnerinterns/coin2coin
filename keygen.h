/*
 * Coin2Coin (https://bitbucket.org/netburnerinterns/coin2coin)
 * Copyright (c) 2015 by
 *
 * Sam Posner (http://arcadeoftheabsurd.com/)
 * and NetBurner, Inc. (http://www.netburner.com/)
 *
 * Distributed under the BSD 3-Clause License
 * See LICENSE.txt or visit http://opensource.org/licenses/BSD-3-Clause
 */

#ifndef KEYGEN_H_
#define KEYGEN_H_

void StartKeygenTask();
void WaitKeygenTask();
char* GetWIFKey();
char* GetMiniPrivateKey();
char* GetAddress();

#endif /* KEYGEN_H_ */
