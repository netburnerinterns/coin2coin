/*
 * Coin2Coin (https://bitbucket.org/netburnerinterns/coin2coin)
 * Copyright (c) 2015 by
 *
 * Sam Posner (http://arcadeoftheabsurd.com/)
 * and NetBurner, Inc. (http://www.netburner.com/)
 *
 * Distributed under the BSD 3-Clause License
 * See LICENSE.txt or visit http://opensource.org/licenses/BSD-3-Clause
 */

/*
 * Functions for generating Bitcoin addresses
 *
 * See test code in test/test.cpp and documentation at:
 * https://en.bitcoin.it/wiki/Technical_background_of_version_1_Bitcoin_addresses
 */

#ifndef BITCOIN_H_
#define BITCOIN_H_

void GenMiniPrivateKey(char* mpriv, unsigned char* priv);
int GenPublicKey(PBYTE priv, char* out_str);
int PrivToWIF(PBYTE in, int len, char* out_str);

#endif /* BITCOIN_H_ */
