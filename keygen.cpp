/*
 * Coin2Coin (https://bitbucket.org/netburnerinterns/coin2coin)
 * Copyright (c) 2015 by
 *
 * Sam Posner (http://arcadeoftheabsurd.com/)
 * and NetBurner, Inc. (http://www.netburner.com/)
 *
 * Distributed under the BSD 3-Clause License
 * See LICENSE.txt or visit http://opensource.org/licenses/BSD-3-Clause
 */

#include <predef.h>
#include <constants.h>
#include <ucos.h>
#include <iosys.h>
#include <crypto/random.h>

#include "keygen.h"
#include "bitcoin.h"
#include "test/test.h"

#define KEYGEN_STACK_SIZE 262144
#define KEYGEN_TASK_PRIORITY 46

asm( " .align 4 " );

DWORD KeygenStack[KEYGEN_STACK_SIZE] __attribute__( ( aligned( 4 ) ) );
OS_SEM KeygenSemaphore;

static char mpriv[128];
static char wif[128];
static char address[128];

void KeygenTask(void * pdata) {
    unsigned char priv[32];

    iprintf("[Keygen task] Generating keys\n");

    GenMiniPrivateKey(mpriv, priv);
    PrivToWIF(priv, 32, wif);

    iprintf("[Keygen task] Got private key\n");
    //PrintHex(priv, 32);

    GenPublicKey(priv, address);

    iprintf("[Keygen task] Got public key and address\n");

    OSSemPost(&KeygenSemaphore);
}

void StartKeygenTask() {
    OSSemInit(&KeygenSemaphore, 0);

    if (OSTaskCreate(KeygenTask,
                     NULL,
                     (void*)&KeygenStack[KEYGEN_STACK_SIZE],
                     (void *)KeygenStack, KEYGEN_TASK_PRIORITY
                    )!=OS_NO_ERR) {
        iprintf("Error starting task\n");
    }
}

void WaitKeygenTask() {
    OSSemPend(&KeygenSemaphore, 0);
}

char* GetWIFKey() {
    return wif;
}

char* GetMiniPrivateKey() {
    return mpriv;
}

char* GetAddress() {
    return address;
}
