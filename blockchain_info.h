/*
 * Coin2Coin (https://bitbucket.org/netburnerinterns/coin2coin)
 * Copyright (c) 2015 by
 *
 * Sam Posner (http://arcadeoftheabsurd.com/)
 * and NetBurner, Inc. (http://www.netburner.com/)
 *
 * Distributed under the BSD 3-Clause License
 * See LICENSE.txt or visit http://opensource.org/licenses/BSD-3-Clause
 */

/*
 * Library to compose requests and parse responses from
 * the blockchain.info api: https://blockchain.info/api
 *
 * Define blockchain.info GUID and password in apikeys.h
 */

#ifndef BLOCKCHAIN_INFO_H_
#define BLOCKCHAIN_INFO_H_

#include "lib/cjson/cJSON.h"

#define BLOCKCHAIN_HOST "blockchain.info"

void GetAddressBalanceRequest(char out[], char* address, int confirmations);
void GetWalletBalanceRequest(char out[]);
void GetPaymentRequest(char out[], char* address, long amount);
void GetPriceRequest(char out[], char* currency, char* value);

double ParsePrice(char* rx_buffer);
double ParseBalance(cJSON* root);
bool ParseError(cJSON* root);

//void PostPaymentRequest(char out[], char* address, char* amount);

#endif /* BLOCKCHAIN_INFO_H_ */
