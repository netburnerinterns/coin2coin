/*
 * Adapted from libbase58 (https://github.com/luke-jr/libbase58)
 * Copyright 2012-2014 Luke Dashjr
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the standard MIT license.  See COPYING for more details.
 */

#ifndef LIBBASE58_H
#define LIBBASE58_H

#ifdef __cplusplus
extern "C" {
#endif

extern void b58enc(char *b58, int *b58sz, unsigned char* data, int binsz);

#ifdef __cplusplus
}
#endif

#endif
