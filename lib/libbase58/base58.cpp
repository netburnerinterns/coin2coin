/*
 * Adapted from libbase58 (https://github.com/luke-jr/libbase58)
 * Copyright 2012-2014 Luke Dashjr
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the standard MIT license.  See COPYING for more details.
 */

#include <predef.h>
#include <constants.h>
#include <ucos.h>
#include <system.h>
#include <iosys.h>
#include <string.h>

#include "libbase58.h"

static const char b58digits_ordered[] = "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz";

/*
 * Encodes the given data in a base58 string
 *
 * Hack warning:
 * This function used to do some error checking and return a bool,
 * but it was failing on valid input so I commented that bit out.
 */
void b58enc(char *b58, int *b58sz, unsigned char* data, int binsz) {
    const uint8_t *bin = data;
    int carry;
    int i, j, high, zcount = 0;
    int size;

    while (zcount < binsz && !bin[zcount])
        ++zcount;

    size = (binsz - zcount) * 138 / 100 + 1;
    uint8_t buf[size];
    memset(buf, 0, size);

    for (i = zcount, high = size - 1; i < binsz; ++i, high = j) {
        for (carry = bin[i], j = size - 1; (j > high) || carry; --j) {
            carry += 256 * buf[j];
            buf[j] = carry % 58;
            carry /= 58;
        }
    }

    for (j = 0; j < size && !buf[j]; ++j);

    /*if (*b58sz <= zcount + size - j) {
    	*b58sz = zcount + size - j + 1;
    	return false;
    }*/

    if (zcount) {
        memset(b58, '1', zcount);
    }

    for (i = zcount; j < size; ++i, ++j) {
        b58[i] = b58digits_ordered[buf[j]];
    }

    b58[i] = '\0';
    *b58sz = i + 1;

    //return true;
}
