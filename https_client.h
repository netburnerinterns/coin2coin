/*
 * Coin2Coin (https://bitbucket.org/netburnerinterns/coin2coin)
 * Copyright (c) 2015 by
 *
 * Sam Posner (http://arcadeoftheabsurd.com/)
 * and NetBurner, Inc. (http://www.netburner.com/)
 *
 * Distributed under the BSD 3-Clause License
 * See LICENSE.txt or visit http://opensource.org/licenses/BSD-3-Clause
 */

/*
 * Based on NetBurner SSL HTTP GET Request Example
 */

#ifndef HTTPS_H_
#define HTTPS_H_

#include "lib/cjson/cJSON.h"

cJSON* GetJSON(char* rx_buffer);
void HTTPSRequest(char* name, char* request, char* rx_buffer, int rx_len);

#endif /* HTTPS_H_ */
