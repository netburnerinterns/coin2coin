/*
 * Coin2Coin (https://bitbucket.org/netburnerinterns/coin2coin)
 * Copyright (c) 2015 by
 *
 * Sam Posner (http://arcadeoftheabsurd.com/)
 * and NetBurner, Inc. (http://www.netburner.com/)
 *
 * Distributed under the BSD 3-Clause License
 * See LICENSE.txt or visit http://opensource.org/licenses/BSD-3-Clause
 */

/*
 * Functions for generating Bitcoin addresses
 *
 * See test code in test/test.cpp and documentation at:
 * https://en.bitcoin.it/wiki/Technical_background_of_version_1_Bitcoin_addresses
 */

#include <predef.h>
#include <constants.h>
#include <ucos.h>
#include <iosys.h>
#include <crypto/random.h>
#include <crypto/SHA2.h>
#include <string.h>
#include <math.h>

#include "lib/libbase58/libbase58.h"
#include "lib/secp256k1/secp256k1.h"
#include "lib/bitcoin/crypto/ripemd160.h"
#include "test/test.h"

static const char chars[] = "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz";

/*
 * Returns a random unsigned 16-bit integer between 0 and limit, exclusive
 *
 * See crypto.stackexchange post for math:
 * http://crypto.stackexchange.com/questions/7996/correct-way-to-map-random-number-to-defined-range
 */
uint16_t GetRandomInt(uint16_t limit) {
    uint16_t range = limit * (USHRT_MAX / limit);
    uint16_t candidate;
    while ((candidate = GetRandomWord()) >= range) {}

    return candidate % limit;
}

/*
 * Generates a mini private key and the corresponding full private key
 */
void GenMiniPrivateKey(char* mpriv, unsigned char* priv) {
    unsigned char candidate[128];
    unsigned char hash[32];

    bool found = false;
    SHA2_CTX ctx_sha;

    candidate[0] = 'S';
    candidate[30] = '?';

    while (!found) {
        for (int i = 1; i < 30; i++) {
            candidate[i] = chars[GetRandomInt(58)];
        }

        SHA2Init(&ctx_sha, false);
        SHA2Update(&ctx_sha, candidate, 31);
        SHA2Final(hash, &ctx_sha);

        if (hash[0] == 0x00) {
            found = true;
        }
    }

    SHA2Init(&ctx_sha, false);
    SHA2Update(&ctx_sha, candidate, 30);
    SHA2Final(hash, &ctx_sha);

    memcpy(mpriv, &candidate, 30);
    memcpy(priv, &hash, 32);
}

/*
 * Generates the Bitcoin address of a 32-byte private key
 * Returns the length of the address string, or -1 on failure
 */
int GenPublicKey(PBYTE priv, char* out_str) {
    unsigned char pub[65];
    unsigned char hash_sha[32];
    unsigned char hash_ripemd[25];

    SHA2_CTX ctx_sha;
    secp256k1_context_t* ctx_ecc;
    CRIPEMD160 ctx_ripemd;

    int pub_len = 0;
    int out_len;

    // ECDSA public key

    ctx_ecc = secp256k1_context_create(SECP256K1_CONTEXT_SIGN);

    if (!secp256k1_ec_pubkey_create(ctx_ecc, pub, &pub_len, priv, false)) {
        return -1;
    }

    secp256k1_context_destroy(ctx_ecc);

    // SHA-256 hash

    SHA2Init(&ctx_sha, false);
    SHA2Update(&ctx_sha, pub, pub_len);
    SHA2Final(hash_sha, &ctx_sha);

    // RIPEMD-160 hash

    ctx_ripemd = CRIPEMD160();
    ctx_ripemd.Write(hash_sha, 32);
    ctx_ripemd.Finalize(hash_ripemd + 1);

    hash_ripemd[0] = 0x00;

    // Base58Check

    SHA2Init(&ctx_sha, false);
    SHA2Update(&ctx_sha, hash_ripemd, 21);
    SHA2Final(hash_sha, &ctx_sha);

    SHA2Init(&ctx_sha, false);
    SHA2Update(&ctx_sha, hash_sha, 32);
    SHA2Final(hash_sha, &ctx_sha);

    // checksum
    hash_ripemd[21] = hash_sha[0]; hash_ripemd[22] = hash_sha[1];
    hash_ripemd[23] = hash_sha[2]; hash_ripemd[24] = hash_sha[3];

    b58enc(out_str, &out_len, hash_ripemd, 25);

    return out_len;
}

/*
 * Encodes the given private key in wallet import format
 * Returns the length of the encoded string
 */
int PrivToWIF(PBYTE in, int len, char* out_str) {
    unsigned char key[len + 5];
    unsigned char hash[32];

    SHA2_CTX ctx_sha;
    int out_len;

    // prepend version byte
    key[0] = 0x80;
    memcpy(key + 1, in, len);

    // Base58Check

    SHA2Init(&ctx_sha, false);
    SHA2Update(&ctx_sha, key, len + 1);
    SHA2Final(hash, &ctx_sha);

    SHA2Init(&ctx_sha, false);
    SHA2Update(&ctx_sha, hash, 32);
    SHA2Final(hash, &ctx_sha);

    // append checksum
    key[len + 1] = hash[0];
    key[len + 2] = hash[1];
    key[len + 3] = hash[2];
    key[len + 4] = hash[3];

    b58enc(out_str, &out_len, key, len + 5);

    return out_len;
}
