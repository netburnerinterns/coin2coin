# __Insert Coin; Receive Bitcoin__

Source for a basic Bitcoin vending machine implemented on NetBurner's MOD5441x platform
 
May be useful as an example of doing Bitcoin-related cryptography on embedded platforms,  
calling the blockchain.info API from C, or for the QR code printing function

### Functionality

* Wait for coin pulse
* Generate new Bitcoin address
* Transfer .25 USD in Bitcoin to address
* Print QR codes of private key and public address

### Useful Code

* Functions to generate Bitcoin addresses and mini private keys (`bitcoin.cpp`)
* Blockchain.info API implementation (`blockchain_info.cpp`)
* Function to print libqrencode QR codes with the CSN-A2-T thermal printer (`printer.cpp`)

### Libraries

* secp256k1 ECDSA: [libsecp256k1](https://github.com/bitcoin/secp256k1)
* RIPEMD-160: [Bitcoin Core implementation](https://github.com/bitcoin/bitcoin/blob/master/src/crypto/ripemd160.cpp)
* Base58Check: [libbase58](https://github.com/luke-jr/libbase58)
* SHA-256: NetBurner cryptolib
* QR Encoding: [libqrencode](https://github.com/fukuchi/libqrencode)
* JSON Parsing: [cjson](https://github.com/kbranigan/cJSON)

### Hardware

* [CSN-A2-T thermal printer](http://www.adafruit.com/product/597)
* [CH-924 coin acceptor](http://www.adafruit.com/product/787)
* [NetBurner MOD5441X development kit](http://www.netburner.com/products/core-modules/mod5441x#kit)

### Notes

Coin2Coin copyright (c) 2015 by  
 
Sam Posner (http://arcadeoftheabsurd.com/)  
and NetBurner, Inc. (http://www.netburner.com/)

Distributed under the BSD 3-Clause License  
See LICENSE.txt or visit http://opensource.org/licenses/BSD-3-Clause