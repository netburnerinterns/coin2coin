/*
 * Coin2Coin (https://bitbucket.org/netburnerinterns/coin2coin)
 * Copyright (c) 2015 by
 *
 * Sam Posner (http://arcadeoftheabsurd.com/)
 * and NetBurner, Inc. (http://www.netburner.com/)
 *
 * Distributed under the BSD 3-Clause License
 * See LICENSE.txt or visit http://opensource.org/licenses/BSD-3-Clause
 */

/*
 * Based on NetBurner SSL HTTP GET Request Example
 */

#include "predef.h"
#include <stdio.h>
#include <ctype.h>
#include <startnet.h>
#include <autoupdate.h>
#include <tcp.h>
#include <string.h>
#include <crypto/ssl.h>
#include <debugtraps.h>
#include <iointernal.h>
#include <dns.h>
#include <dhcpclient.h>

#include "https_client.h"

/*
 * Returns the JSON data in a response string
 */
cJSON* GetJSON(char* rx_buffer) {
    char* ptr = strchr(rx_buffer, '{');

    if(ptr) {
        return cJSON_Parse(ptr);
    }

    return 0;
}

/*
 * Executes an HTTP GET request over SSL
 */
void HTTPSRequest(char* name, char* request, char* rx_buffer, int rx_len) {
    iprintf( "Getting [%s]\r\n", name );
    IPADDR ipa = 0;
    int result = GetHostByName( name, &ipa, 0, 40 );

    if ( ( result == DNS_OK ) && ( ipa != 0 ) ) {
        iprintf( "\r\n%s = ", name ); ShowIP( ipa );
        iprintf( "\r\n" );

        int fds = SSL_connect( ipa, 0, 443, 400, name );

        if ( fds <= 0 ) {
            iprintf( "Retry\r\n" );
            fds = SSL_connect( ipa, 0, 443, 400, name );
        }

        int n;
        int total;

        if ( fds > 0 ) {
            iprintf( "We connected \n" );
            writestring( fds, request );
            total = 0;

            do {
                n = ReadWithTimeout( fds, rx_buffer + total, rx_len - total, 100 );

                if ( n > 0 ) {
                    total += n;
                } else {
                    iprintf( "N response = %d\r\n", n );
                }
            } while ( n > 0 );

            iprintf( "Read %d bytes\r\n", total );
            rx_buffer[total] = 0;
            //iprintf( "[%s]\r\n", rx_buffer );
            OSTimeDly( 20 );
            close( fds );
        } else {
            iprintf( "Connect failed with %d :", fds );

            switch ( fds ) {
            case SSL_ERROR_FAILED_NEGOTIATION       :
                iprintf( " SSL_ERROR_FAILED_NEGOTIATION        \r\n" ); break;

            case SSL_ERROR_HASH_FAILED              :
                iprintf( " SSL_ERROR_HASH_FAILED               \r\n" ); break;

            case SSL_ERROR_CERTIFICATE_UNKNOWN      :
                iprintf( " SSL_ERROR_CERTIFICATE_UNKNOWN       \r\n" ); break;

            case SSL_ERROR_WRITE_FAIL               :
                iprintf( " SSL_ERROR_WRITE_FAIL                \r\n" ); break;

            case SSL_ERROR_CERTIFICATE_NAME_FAILED  :
                iprintf( " SSL_ERROR_CERTIFICATE_NAME_FAILED   \r\n" ); break;

            case SSL_ERROR_CERTIFICATE_VERIFY_FAILED:
                iprintf( " SSL_ERROR_CERTIFICATE_VERIFY_FAILED \r\n" ); break;

            default:
                iprintf( "Other error\r\n" );
            }
        }
    } else {
        iprintf( "Failed to get DNS entry for %s, verify the DNS server IP address is correct\r\n",
                 name );
    }
}
